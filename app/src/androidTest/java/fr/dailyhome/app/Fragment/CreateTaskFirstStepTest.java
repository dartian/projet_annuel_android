package fr.dailyhome.app.Fragment;

import android.test.AndroidTestCase;

import java.util.Date;

import fr.dailyhome.app.Model.Task;


/**
 * Created by bidau on 25/03/2016.
 */
public class CreateTaskFirstStepTest extends AndroidTestCase {

    public void testIsValid() throws Exception {
        CreateTaskFirstStep createTaskFirstStep = new CreateTaskFirstStep();
        Task task = new Task();
        task.setDate(new Date());
        createTaskFirstStep.setNewTask(task);
        assertEquals(true, createTaskFirstStep.isValid());
    }
}
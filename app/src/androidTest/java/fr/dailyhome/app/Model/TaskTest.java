package fr.dailyhome.app.Model;

import android.test.AndroidTestCase;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bidau on 24/03/2016.
 */
public class TaskTest extends AndroidTestCase {

    public void testGetDateFormatage() throws Exception {
        Task task = new Task();
        Date date = new Date();
        task.setDate(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        assertEquals(simpleDateFormat.format(date), task.getDateFormalise());
    }
}
package fr.dailyhome.app.module;

import fr.dailyhome.app.Model.Search;
import io.realm.annotations.RealmModule;

/**
 * Created by d.bidaud on 20/04/2016.
 */
@RealmModule(classes = {Search.class})
public class SearchModule {
}

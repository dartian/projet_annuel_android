package fr.dailyhome.app.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import fr.dailyhome.app.R;

/**
 * Created by Damien on 05/01/2016.
 */
public class TaskViewHolder extends RecyclerView.ViewHolder{

    protected TextView title;
    protected TextView date;
    protected TextView userName;
    protected TextView state;

    public TaskViewHolder(View itemView) {
        super(itemView);
        this.title = (TextView) itemView.findViewById(R.id.title_tache);
        this.date = (TextView) itemView.findViewById(R.id.date_tache);
        this.userName = (TextView) itemView.findViewById(R.id.user_tache);
        this.state = (TextView) itemView.findViewById(R.id.task_state);
    }
}

package fr.dailyhome.app.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.R;

/**
 * Created by Damien on 05/01/2016.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {


    private List<Task> taskList;
    private String[] typeTask;
    private String[] taskState;

    public TaskAdapter(List<Task> modelList, String[] typeTask, String[] taskState){
        this.taskList = modelList;
        this.typeTask = typeTask;
        this.taskState = taskState;
    }


    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);

        return new TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Task task = taskList.get(position);
        holder.title.setText(this.typeTask[task.getTaskType()]);
        holder.date.setText(task.getDateFormalise());
        holder.state.setText(this.taskState[task.getState_mission()]);
        //tring name = FragmentTask.getClient().getFirst_name();
        //holder.userName.setText(name);
    }

    @Override
    public int getItemCount() {
        if(this.taskList!=null)
            return this.taskList.size();
        else
            return 0;
    }

}

package fr.dailyhome.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.INetworkObservable;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.UserService;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class Login extends AppCompatActivity implements INetworkObservable {

    private EditText login;
    private EditText password;
    private Button valid;
    private User newUser;
    private Realm realm;
    private AppiConnexion appiConnexion;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appiConnexion=  new AppiConnexion();

        this.login = (EditText) findViewById(R.id.login);
        this.password = (EditText) findViewById(R.id.password);
        this.valid = (Button) findViewById(R.id.button2);

        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext()).name("user.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> results = query.findAll();
        if(results.size()>0) {
            Intent intent = new Intent(getApplicationContext(), ListTache.class);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }


    public void logIsCorrect(final String login, String password){
        /**
         Launch the server call for check the login and password and lunch the app if is correct
         */
        final CharSequence error = "Missing argument";
        //Gson gson = new GsonBuilder().registerTypeAdapter(User.class, new UserAdapter()).create();

        connectionUtility = new ConnectionUtility(getApplicationContext());
        if(connectionUtility.checkConnection()) {
            UserService userService = appiConnexion.getRetrofit().create(UserService.class);
            Observable<User> user = userService.authenticateUser(login, password);

            user.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<User>() {
                        @Override
                        public void onCompleted() {
                            Intent intent = new Intent(getApplicationContext(), ListTache.class);

                            realm.beginTransaction();
                            final User saveUser = realm.copyToRealm(newUser);
                            realm.commitTransaction();

                            if (newUser.getType() == 1) {
                                intent.putExtra("type", "pro");
                            } else {
                                intent.putExtra("type", "client");
                            }
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(getApplicationContext(),
                                    R.string.error_message,
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onNext(User user) {
                            newUser = user;
                        }
                    });
        }else{
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.loginActivity), "Welcome to AndroidHive", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    public void validateLogin(View view) {
        logIsCorrect(this.login.getText().toString(), this.password.getText().toString());
    }

    public void signIn(View view){
        startActivity(new Intent(this, Sign.class));
    }

    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.loginActivity);
        if(code == 0 && !change) {
            Log.i("list", "connecter");
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Connection retrouvé", Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.GREEN);
            snackbar.show();
            change = true;

        }else if(code == 1){
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            if(coordinatorLayout.getContext()!=null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Pas de connection", Snackbar.LENGTH_INDEFINITE);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.RED);
                snackbar.show();
                change = false;
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }
}

package fr.dailyhome.app.Fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import fr.dailyhome.app.ListTache;
import fr.dailyhome.app.Model.Search;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.R;
import fr.dailyhome.app.TaskActivity;
import fr.dailyhome.app.ViewHolder.TaskAdapter;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.RecyclerItemClickListener;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.TaskNetwork;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class searchResultList extends Fragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private GoogleApiClient googleApiClient;
    private Location location;
    private boolean locationState;
    private int type;
    private AppiConnexion appiConnexion;


    private ArrayList<Task> taskList;

    public searchResultList() {
        // Required empty public constructor
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_search_result_list, container, false);
        context = view.getContext();

        appiConnexion = new AppiConnexion();
        recyclerView = (RecyclerView) view.findViewById(R.id.listSearchResult);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener(){

                    @Override
                    public void onItemClick(View view, int position) {

                        /*Intent intent = new Intent(view.getContext(), TaskActivity.class);
                        intent.putExtra("FragmentTask", FragmentTask);
                        startActivity(intent);*/
                        Task task = taskList.get(position);
                        Intent intent = new Intent(view.getContext(), TaskActivity.class);
                        intent.putExtra("task", task);
                        startActivity(intent);
                    }
                })
        );

        Bundle bundle = this.getArguments();
        Search searchValue = bundle.getParcelable("search");
        locationState = bundle.getBoolean("location");
        type = bundle.getInt("type");

        if (googleApiClient == null) {

            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }else{
            Log.i("GPS FRAGMENT", "connecter");
        }

        if(!locationState){
            getSearchLocation(searchValue);
        }


        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("GPS FRAGMENT", "Location services connected.");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            Log.i("FRAGMENT GPS", "fsdlk");
            handleNewLocation(location);
        }else{
            Log.i("FRAGMENT GPS", "not");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    if(location!= null){
                        handleNewLocation(location);
                    }else{
                        Toast.makeText(context,
                                "GPS d'ésactivé",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                }
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Demande au serveur les recherche possible avec la geolocalisation
     * @param location
     */
    private void handleNewLocation(Location location){
        Double longitude = location.getLongitude();
        Double latitude = location.getLatitude();
        if(locationState){
            ConnectionUtility connectionUtility = new ConnectionUtility(context);
            if(connectionUtility.checkConnection()) {
                TaskNetwork userService = appiConnexion.getRetrofit().create(TaskNetwork.class);
                Observable<List<Task>> user = userService.getSearchFromPosition(latitude, longitude, type);

                user.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<List<Task>>() {
                            @Override
                            public void onCompleted() {
                                Intent intent = new Intent(context, ListTache.class);


                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(context,
                                        R.string.error_message,
                                        Toast.LENGTH_SHORT)
                                        .show();
                                Log.i("error", e.getMessage());
                            }

                            @Override
                            public void onNext(List<Task> tasks) {
                                String[] type = context.getResources().getStringArray(R.array.taskType);
                                String[] state = context.getResources().getStringArray(R.array.state);
                                TaskAdapter taskAdapter = new TaskAdapter(tasks, type, state);
                                recyclerView.setAdapter(taskAdapter);
                                taskList = (ArrayList<Task>) tasks;
                            }

                        });
            }
        }
    }

    /**
     * Demande au serveur les recherche possible avec une adresse
     * @param search
     */
    private void getSearchLocation(Search search){
        ConnectionUtility connectionUtility = new ConnectionUtility(context);
        if(connectionUtility.checkConnection()) {
            TaskNetwork userService = appiConnexion.getRetrofit().create(TaskNetwork.class);
            Log.i("test", search.getSearch());
            Observable<List<Task>> user = userService.getSearchFromAddress(search.getSearch(), type);

            user.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<List<Task>>() {
                        @Override
                        public void onCompleted() {
                            Intent intent = new Intent(context, ListTache.class);


                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context,
                                    R.string.error_message,
                                    Toast.LENGTH_SHORT)
                                    .show();
                            Log.i("error", e.getMessage());
                        }

                        @Override
                        public void onNext(List<Task> tasks) {
                            String[] type = context.getResources().getStringArray(R.array.taskType);
                            String[] state = context.getResources().getStringArray(R.array.state);
                            TaskAdapter taskAdapter = new TaskAdapter(tasks, type, state);
                            recyclerView.setAdapter(taskAdapter);
                            taskList = (ArrayList<Task>) tasks;
                        }

                    });
        }
    }
}

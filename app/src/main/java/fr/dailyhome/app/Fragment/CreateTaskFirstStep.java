package fr.dailyhome.app.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.DatePickerFragment;
import fr.dailyhome.app.helper.Dates;
import fr.dailyhome.app.helper.TimePickerFragment;
import fr.dailyhome.app.network.AddressService;
import fr.dailyhome.app.network.AppiConnexion;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class CreateTaskFirstStep extends Fragment {

    private Calendar calendar = Calendar.getInstance();
    private Button dateSelect;
    private Button timeSelect;
    private Button validTask;
    private Spinner typeTask;
    private Spinner period;
    private Spinner repete;
    private EditText particularity;
    private RadioGroup radioGroupAddress;
    private Realm realm;
    private View view;
    private Context context;
    private  Task newTask;
    private CheckBox ponctuel;


    public static final int DIALOG_FRAGMENT = 1;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_create_task_first_step, container, false);
        this.context = view.getContext();
        realm = Realm.getDefaultInstance();
        dateSelect = (Button) view.findViewById(R.id.newTaskDate);
        timeSelect = (Button) view.findViewById(R.id.newTaskTime);
        typeTask = (Spinner) view.findViewById(R.id.newTaskType);
        period = (Spinner) view.findViewById(R.id.period);
        repete = (Spinner) view.findViewById(R.id.repete_spinner);
        validTask = (Button) view.findViewById(R.id.button);
        particularity = (EditText) view.findViewById(R.id.particularity_input);
        radioGroupAddress = (RadioGroup) view.findViewById(R.id.addressChoice);
        ponctuel = (CheckBox) view.findViewById(R.id.IsPonctuel);
        final Date minDate = new Date();
        dateSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment date = new DatePickerFragment(minDate.getTime()-1000, 0) {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        calendar.set(y, m, d);
                        dateSelect.setText(Dates.dateFormat.format(calendar.getTime()));

                    }
                };

                date.show(getFragmentManager().beginTransaction(), "datePicker");
            }
        });

        timeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment time = new TimePickerFragment() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        timeSelect.setText(Dates.timeFormat.format(calendar.getTime()));
                    }
                };
                time.show(getFragmentManager(), "timePicker");
            }
        });

        validTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initTask();

            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.taskType, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeTask.setAdapter(adapter);

        ArrayAdapter<CharSequence> periodAdapter = ArrayAdapter.createFromResource(context, R.array.period, android.R.layout.simple_spinner_dropdown_item);
        periodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        period.setAdapter(periodAdapter);

        ArrayAdapter<CharSequence> repeteAdapter = ArrayAdapter.createFromResource(context, R.array.repete, android.R.layout.simple_spinner_dropdown_item);
        repeteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        repete.setAdapter(repeteAdapter);

        ponctuel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    repete.setEnabled(false);
                    repete.setClickable(false);
                }else{
                    repete.setEnabled(true);
                    repete.setClickable(true);
                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     *
     */
    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     *
     */
    @Override
    public void onStop(){
        super.onStop();
    }

    /**
     * vérifie si la date et l'address est rensigner
     * @return
     */
    public boolean isValid(){
        return this.newTask.getDate()!= null && this.newTask.getAddress()!=null;
    }

    /**
     *
     * @param task
     */
    public void setNewTask(Task task){
        this.newTask = task;
    }


    /**
     * vérifie si les donnée on été bien renseigner et lance la deuxième étapes de la création
     */
    private void initTask(){
        newTask = new Task();
        newTask.setName("");
        newTask.setDetail(particularity.getText().toString());
        newTask.setTaskType(typeTask.getSelectedItemPosition());
        newTask.setTime_period(period.getSelectedItemPosition());
        if(ponctuel.isChecked())
            newTask.setRepeat_type(-1);
        else
            newTask.setRepeat_type(repete.getSelectedItemPosition());
        String dateText = dateSelect.getText().toString();
        String TimeText = timeSelect.getText().toString();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        String dateInString = dateText+" "+TimeText+":00";
        Date date = null;
        try {
            date = formatter.parse(dateInString);
            newTask.setDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> result1 = query.findAll();
        User user = result1.first();
        newTask.setClient(user.getId());
        if(radioGroupAddress.getCheckedRadioButtonId()!=-1) {
            switch (radioGroupAddress.getCheckedRadioButtonId()) {
                case R.id.myadressradio:
                    //newTask.setId_address();
                    RealmQuery<Address> query1 = realm.where(Address.class);
                    RealmResults<Address> results2 = query1.findAll();
                    Address addressUser = results2.last();
                    newTask.setAddress(addressUser);
                    if(isValid()) {
                        senDate();
                    }else{
                        displayAlert();
                    }
                    break;
                case R.id.otherAdressRadio:
                    if (newTask.getAddress() == null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        DialogueAddress dialogueAddress = new DialogueAddress();
                        dialogueAddress.setTargetFragment(this, DIALOG_FRAGMENT);
                        dialogueAddress.show(fragmentManager, "dialogueAddress");
                    }
                    break;
            }
        }else{
            if(isValid()) {
                senDate();
            }else{
                displayAlert();
            }
        }

    }


    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode){
            case DIALOG_FRAGMENT:
                if(resultCode == Activity.RESULT_OK){
                    final Address address = data.getParcelableExtra("address");
                    ConnectionUtility connectionUtility = new ConnectionUtility(context);
                    if(connectionUtility.checkConnection()) {
                        AppiConnexion appiConnexion = new AppiConnexion();
                        AddressService addressService = appiConnexion.getRetrofit().create(AddressService.class);
                        RealmQuery<User> query = realm.where(User.class);
                        RealmResults<User> result1 = query.findAll();
                        User user = result1.first();
                        Log.i("address", "id user:" + user.getId());
                        Observable<Integer> id_address = addressService.createAddress(address.getZipCode(), address.getCity(), address.getAddress(), user.getId());

                        id_address.subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .unsubscribeOn(Schedulers.io())
                                .timeout(1, TimeUnit.MINUTES)
                                .subscribe(new Subscriber<Integer>() {

                                    @Override
                                    public void onCompleted() {
                                        if (isValid()) {
                                            Log.i("address", "ok");
                                            senDate();
                                        } else {
                                            displayAlert();
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Toast.makeText(context,
                                                R.string.error_message,
                                                Toast.LENGTH_SHORT)
                                                .show();
                                        Log.i("Address", e.getMessage());
                                    }

                                    @Override
                                    public void onNext(Integer integer) {
                                        Log.i("address", "id address:" + integer);
                                        address.setId(integer);
                                        newTask.setAddress(address);
                                    }
                                });
                    }
                }
                break;
        }
    }

    /**
     *
     */
    private void senDate(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("newTask", newTask);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        CreateTaskSecondStep createTaskSecondStep = new CreateTaskSecondStep();
        createTaskSecondStep.setArguments(bundle);
        fragmentTransaction.replace(R.id.createTaskLayout, createTaskSecondStep);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     *
     */
    private void displayAlert(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context);

        dlgAlert.setMessage(R.string.AlertMessageFirstStep);
        dlgAlert.setTitle(R.string.titleAlertFirstStep);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();

        dlgAlert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
}

package fr.dailyhome.app.Fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.R;


public class DialogueAddress extends DialogFragment {

    private View view;
    private Context context;
    private EditText address;
    private EditText city;
    private EditText cp;
    private Address addressReturn;
    private Button valide;

    public static final int DIALOG_FRAGMENT = 1;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        addressReturn = new Address();
        this.view = inflater.inflate(R.layout.fragment_dialogue_address, container, false);
        this.context = view.getContext();
        this.address = (EditText)view.findViewById(R.id.addressNew);
        this.city = (EditText) view.findViewById(R.id.cityNew);
        this.cp = (EditText) view.findViewById(R.id.postalCodeNew);
        this.valide = (Button) view.findViewById(R.id.createAddress);



        getDialog().setTitle(R.string.address_dialogue_title);

        this.valide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressReturn.setAddress(address.getText().toString());
                addressReturn.setCity(city.getText().toString());
                addressReturn.setZipCode(cp.getText().toString());
                Intent intent = new Intent();
                Log.i("address", addressReturn.toString());
                intent.putExtra("address", addressReturn);
                getTargetFragment().onActivityResult(DIALOG_FRAGMENT, Activity.RESULT_OK, intent);
                getDialog().dismiss();
            }
        });

        return this.view;
    }

    /**
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     *
     */
    @Override
    public void onDetach() {
        super.onDetach();
    }
}

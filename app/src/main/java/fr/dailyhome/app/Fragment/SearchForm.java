package fr.dailyhome.app.Fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import fr.dailyhome.app.Model.Search;
import fr.dailyhome.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchForm extends Fragment {


    private View view;
    private Context context;
    private EditText search;
    private Spinner type;
    private Button searchButton;
    private Search searchValue;
    private boolean location;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_search_form, container, false);
        this.context = view.getContext();
        this.search = (EditText)view.findViewById(R.id.search);
        this.type = (Spinner)view.findViewById(R.id.typeSearch);
        this.searchButton = (Button)view.findViewById(R.id.searchButton);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.taskType, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(adapter);
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            Log.i("test", "ok");
            searchValue = bundle.getParcelable("search");
            location = bundle.getBoolean("location");

        }else{
            searchValue = new Search();
        }
        type.setSelection(searchValue.getType());
        if(!searchValue.getSearch().equals(""))
            search.setText(searchValue.getSearch());
        /**
         * navigue vers la liste de recherche
         */
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                searchValue.setType(type.getSelectedItemPosition());
                searchValue.setSearch(search.getText().toString());
                Bundle bundle = new Bundle();
                bundle.putParcelable("search", searchValue);
                ListSearch listSearch = new ListSearch();
                listSearch.setArguments(bundle);
                fragmentTransaction.replace(R.id.createTaskLayout, listSearch);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSearch();
            }
        });


        return this.view;
    }

    /**
     * navigue vers le fragment d'affichage de recherche
     */
    private void getSearch(){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(location){
            Log.i("test", "ok");
        }else{
            Log.i("tets", "not ok");
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable("search", searchValue);
        bundle.putBoolean("location", location);
        bundle.putInt("type", type.getSelectedItemPosition());
        searchResultList searchResultList = new searchResultList();
        searchResultList.setArguments(bundle);
        fragmentTransaction.replace(R.id.createTaskLayout, searchResultList);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

}

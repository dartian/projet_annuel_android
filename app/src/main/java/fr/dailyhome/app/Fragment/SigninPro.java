package fr.dailyhome.app.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import fr.dailyhome.app.Login;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.UserService;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class SigninPro extends Fragment {

    private View view;
    private Context context;
    private User user;

    private EditText siret;
    private Button valid;
    public SigninPro() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.view = inflater.inflate(R.layout.fragment_signin, container, false);
        this.context = view.getContext();

        Bundle bundle = this.getArguments();
        user = bundle.getParcelable("User");

        this.siret = (EditText) view.findViewById(R.id.siret);
        this.valid = (Button) view.findViewById(R.id.validPro);


        valid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!siret.getText().toString().equals("")){
                    ConnectionUtility connectionUtility = new ConnectionUtility(context);
                    if(connectionUtility.checkConnection()) {
                        AppiConnexion appiConnexion = new AppiConnexion();
                        UserService userService = appiConnexion.getRetrofit().create(UserService.class);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String date = simpleDateFormat.format(user.getBirthDate());
                        Observable<Boolean> check = userService.signIng(user.getFirst_name(), user.getLast_name(), user.getPhoneNumber(), user.geteMail(), date, user.getPassword(), user.getAddress().getAddress(), user.getAddress().getZipCode(), user.getAddress().getCity(), 1, user.getCivilite(), siret.getText().toString());
                        check.subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .unsubscribeOn(Schedulers.io())
                                .subscribe(new Subscriber<Boolean>() {
                                    @Override
                                    public void onCompleted() {
                                        Toast.makeText(context,
                                                R.string.success_sign,
                                                Toast.LENGTH_SHORT)
                                                .show();
                                        startActivity(new Intent(getActivity(), Login.class));
                                        getActivity().finish();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Toast.makeText(context,
                                                R.string.error_message,
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }

                                    @Override
                                    public void onNext(Boolean user) {

                                    }
                                });
                    }
                }
            }
        });

        return view;
    }

}

package fr.dailyhome.app.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import fr.dailyhome.app.ListTache;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.R;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.TaskNetwork;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/*Created by Thibault Dassonville*/

public class CreateTaskSecondStep extends Fragment {


    private View view;
    private Context context;
    private TextView type;
    private TextView time;
    private TextView address;
    private TextView particularity;
    private TextView price;
    private Button valid;
    private Task task;
    private TextView period;
    private RadioGroup paymentMetod;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.fragment_create_task_second_step, container, false);
        this.context = view.getContext();

        Bundle bundle = this.getArguments();
        task = bundle.getParcelable("newTask");

        this.type = (TextView) this.view.findViewById(R.id.type_mission_resume);
        this.time = (TextView) this.view.findViewById(R.id.time_resume_mission);
        this.address = (TextView) this.view.findViewById(R.id.address_resum_mission);
        this.particularity = (TextView) this.view.findViewById(R.id.particularity_resum_mission);
        this.price = (TextView) this.view.findViewById(R.id.price_resume_mission);
        this.valid = (Button) this.view.findViewById(R.id.createTaskButton);
        this.paymentMetod = (RadioGroup) this.view.findViewById(R.id.paymentMethod);
        this.period = (TextView) this.view.findViewById(R.id.date_resume_mission);

        String[] Tasktypes = context.getResources().getStringArray(R.array.taskType);
        String[] TaskPeriod = context.getResources().getStringArray(R.array.period);

        this.type.setText(Tasktypes[task.getTaskType()]);
        this.time.setText(task.getDateFormalise());
        this.address.setText(task.getAddress().toString());
        this.particularity.setText(task.getDetail());
        this.period.setText(TaskPeriod[task.getTime_period()]);

        switch (this.task.getRepeat_type()){ //Tarif en fonction de la fréquence
            case -1: //mission ponctuelle
                this.price.setText("19,90€");
                break;
            case 0: //mission hebdomadaire
                this.price.setText("17,90€");
                break;
            case 1: //mission mensuelle
                this.price.setText("17,90€");
                break;
            case 2: //mission bi-mensuelle
                this.price.setText("17,90€");
                break;
            default:
                this.price.setText("19,90€");
                break;
        }
        this.task.setPaymentMethod(paymentMetod.getCheckedRadioButtonId());
        this.valid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(paymentMetod.getCheckedRadioButtonId()!=-1) {

                    creatTask();
                   /* startActivity(new Intent(getActivity(), ListTache.class));
                    getActivity().finish();*/
                }else{
                    displayAlert();
                }
            }
        });


        // Inflate the layout for this fragment
        return this.view;
    }

    /**
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     *
     */
    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * affiche une alert
     */
    private void displayAlert(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context);

        dlgAlert.setMessage(R.string.alertMessageSecondSted);
        dlgAlert.setTitle(R.string.alertTitleSecondStep);
        dlgAlert.setPositiveButton("OK", null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();

        dlgAlert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    /**
     * créet la mission et l'envoie au server
     */
    public void creatTask(){
        ConnectionUtility connectionUtility = new ConnectionUtility(context);
        if(connectionUtility.checkConnection()) {
            AppiConnexion appiConnexion = new AppiConnexion();
            TaskNetwork taskNetwork = appiConnexion.getRetrofit().create(TaskNetwork.class);
            int methodPayment = 0;
            switch (this.paymentMetod.getCheckedRadioButtonId()) {
                case R.id.cb:
                    methodPayment = 0;
                    break;
                case R.id.paypal:
                    methodPayment = 1;
                    break;
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = simpleDateFormat.format(this.task.getDate());

            Observable<Boolean> create = taskNetwork.create(this.task.getClient(), this.task.getAddress().getId(), this.task.getTaskType(), this.task.getDetail(), date, methodPayment, this.task.getTime_period(), this.task.getRepeat_type());

            create.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {
                            startActivity(new Intent(getActivity(), ListTache.class));
                            getActivity().finish();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context,
                                    R.string.error_message,
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onNext(Boolean user) {

                        }
                    });
        }
    }


}

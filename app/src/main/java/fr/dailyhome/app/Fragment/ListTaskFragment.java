package fr.dailyhome.app.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.dailyhome.app.ListTache;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import fr.dailyhome.app.TaskActivity;
import fr.dailyhome.app.ViewHolder.TaskAdapter;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.RecyclerItemClickListener;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.TaskNetwork;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A placeholder fragment containing a simple view.
 */
public class ListTaskFragment extends Fragment {

    private String TAG = "LIST_TASK";

    private Context context;
    private ArrayList<Task> taskList;
    private RecyclerView recyclerView;
    private View view;
    private User user;
    private AppiConnexion appiConnexion;
    private SwipeRefreshLayout swipeRefreshLayout;
    //private ProgressBar spinner;

    public ListTaskFragment() {
    }

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_list_tache, container, false);
        context = view.getContext();
        //spinner=(ProgressBar) view.findViewById(R.id.progressBar);
        //spinner.setVisibility(View.VISIBLE);

        appiConnexion = new AppiConnexion();
       /* TaskDAO taskDAO = new TaskDAO(context);
        taskDAO.open();*/
        //taskList = null;
        recyclerView = (RecyclerView) view.findViewById(R.id.listTache);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        //taskDAO.close();
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> result1 = query.findAll();
        user = result1.first();
        //recyclerView.setAdapter(taskAdapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener(){

                    @Override
                    public void onItemClick(View view, int position) {

                        /*Intent intent = new Intent(view.getContext(), TaskActivity.class);
                        intent.putExtra("FragmentTask", FragmentTask);
                        startActivity(intent);*/
                        Task task = taskList.get(position);
                        Intent intent = new Intent(view.getContext(), TaskActivity.class);
                        intent.putExtra("task", task);
                        startActivity(intent);
                    }
                })
        );
        getAppointment();

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAppointment();

            }
        });
        //spinner.setVisibility(View.GONE);
        return view;
    }


    /**
     * charge les missions et init la liste
     */
    public void getAppointment(){
        ConnectionUtility connectionUtility = new ConnectionUtility(context);
        if(connectionUtility.checkConnection()) {
            TaskNetwork userService = appiConnexion.getRetrofit().create(TaskNetwork.class);
            Observable<List<Task>> user = userService.getAppointement(this.user.getId());

            user.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Task>>() {
                    @Override
                    public void onCompleted() {
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context,
                                R.string.error_message,
                                Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onNext(List<Task> tasks) {
                        String[] type = context.getResources().getStringArray(R.array.taskType);
                        String[] state = context.getResources().getStringArray(R.array.state);
                        TaskAdapter taskAdapter = new TaskAdapter(tasks, type, state);
                        recyclerView.setAdapter(taskAdapter);
                        taskList = (ArrayList<Task>) tasks;
                    }

                });

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "ok");
        if(resultCode == Activity.RESULT_OK){

        }

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    @Override
    public void onPause() {
        super.onPause();
    }



    public List<Task> getTask(){
        /**
         * get the activity from the server
         */
        return null;
    }
}

package fr.dailyhome.app.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import fr.dailyhome.app.ListTache;
import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import fr.dailyhome.app.cancel;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.Dates;
import fr.dailyhome.app.network.AddressService;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.TaskNetwork;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTask extends Fragment {

    private TextView type;
    private TextView date;
    private TextView time;
    private TextView state;
    private TextView repeatTask;
    private TextView addressLabel;
    private TextView period;
    private Button button;
    private Task task;
    private View view;
    private Context context;
    private Realm realm;
    private User user;
    private Button refuse;
    public FragmentTask() {
        // Required empty public constructor
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_task, container, false);
        context = view.getContext();

        Bundle bundle = this.getArguments();
        if(bundle!=null)
            task = bundle.getParcelable("task");
        else
            Log.i("Task", "bundle null");

        type = (TextView) view.findViewById(R.id.typeTask);
        date = (TextView) view.findViewById(R.id.dateTask);
        time = (TextView) view.findViewById(R.id.timeTask);
        state = (TextView) view.findViewById(R.id.stateTask);
        repeatTask = (TextView) view.findViewById(R.id.repeatTask);
        period = (TextView) view.findViewById(R.id.periodTask);

        //clientInfo = (TextView) findViewById(R.id.clientInfo);

        if(task != null) {
            String[] typeTask = this.getResources().getStringArray(R.array.taskType);
            String[] stateTask = this.getResources().getStringArray(R.array.state);
            String[] repeatTaskArray = this.getResources().getStringArray(R.array.repete);
            String[] timeTask = this.getResources().getStringArray(R.array.period);
            Log.i("task", task.getTaskType() + "");
            type.setText(new StringBuilder().append(typeTask[task.getTaskType()]));
            date.setText(new StringBuilder().append(Dates.dateFormat.format(task.getDate().getTime())).toString());
            time.setText(new StringBuilder().append(Dates.timeFormat.format(task.getDate().getTime())).toString());
            state.setText(new StringBuilder().append(stateTask[task.getState_mission()]));
            period.setText(timeTask[task.getTime_period()]);
            if(task.getRepeat_type()==-1){
                repeatTask.setText(R.string.ponctuel_value);
            }else{
                repeatTask.setText(repeatTaskArray[task.getRepeat_type()]);
            }
            ///clientInfo.setText(new StringBuilder().append("Par:").append(FragmentTask.getClient().getFirst_name()).append(" "));
            button = (Button) view.findViewById(R.id.cancel);
            refuse = (Button) view.findViewById(R.id.refuse_button);

            realm = Realm.getDefaultInstance();
            RealmQuery<User> query = realm.where(User.class);
            RealmResults<User> result1 = query.findAll();
            user = result1.first();
            addressLabel = (TextView) view.findViewById(R.id.addressTask);
            final ConnectionUtility connectionUtility = new ConnectionUtility(context);

                AppiConnexion appiConnexion = new AppiConnexion();
                AddressService addressService = appiConnexion.getRetrofit().create(AddressService.class);
            if(connectionUtility.checkConnection()) {
                Observable<Address> addressObservable = addressService.getAddress(task.getId_address());
                addressObservable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<Address>() {

                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(context,
                                        R.string.error_message,
                                        Toast.LENGTH_SHORT)
                                        .show();
                                Log.i("task", e.getMessage());
                            }

                            @Override
                            public void onNext(Address address) {
                                task.setAddress(address);
                                addressLabel.setText(address.toString());
                            }
                        });
            }
                if (user.getType() == 1) {
                    /**
                     * si l'utilisateur est un professionel
                     */
                    if (task.getState_mission() == 0) {
                        /**
                         * si l'état de la mission est en cours
                         */
                        button.setText("Valider");
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(connectionUtility.checkConnection()) {
                                    AppiConnexion appiConnexion = new AppiConnexion();
                                    TaskNetwork taskNetwork = appiConnexion.getRetrofit().create(TaskNetwork.class);
                                    Observable<Boolean> accepted = taskNetwork.accepted(user.getId(), task.getId());
                                    accepted.subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .unsubscribeOn(Schedulers.io())
                                            .subscribe(new Subscriber<Boolean>() {
                                                @Override
                                                public void onCompleted() {
                                                    startActivity(new Intent(context.getApplicationContext(), ListTache.class));
                                                    getActivity().finish();
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    Toast.makeText(context.getApplicationContext(),
                                                            R.string.error_message,
                                                            Toast.LENGTH_SHORT)
                                                            .show();
                                                    Log.i("task", e.getMessage());
                                                }

                                                @Override
                                                public void onNext(Boolean user) {
                                                    Log.i("task", "ok");
                                                }
                                            });
                                }
                            }

                        });
                    } else {
                        ViewGroup layout = (ViewGroup) button.getParent();
                        if (layout != null)
                            layout.removeView(button);
                    }
                    Log.i("task", task.getId_pro()+"="+user.getId()+", "+task.getState_mission());
                    if(task.getId_pro()==user.getId() && task.getState_mission()==0){
                        /**
                         * si la mission à été changer par le client on affiche le bouton refuser
                         */
                        refuse.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(connectionUtility.checkConnection()) {
                                    AppiConnexion appiConnexion = new AppiConnexion();
                                    TaskNetwork taskNetwork = appiConnexion.getRetrofit().create(TaskNetwork.class);
                                    Observable<Boolean> accepted = taskNetwork.refuse(task.getId());
                                    accepted.subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .unsubscribeOn(Schedulers.io())
                                            .subscribe(new Subscriber<Boolean>() {
                                                @Override
                                                public void onCompleted() {
                                                    startActivity(new Intent(context.getApplicationContext(), ListTache.class));
                                                    getActivity().finish();
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    Toast.makeText(context.getApplicationContext(),
                                                            R.string.error_message,
                                                            Toast.LENGTH_SHORT)
                                                            .show();
                                                    Log.i("task", e.getMessage());
                                                }

                                                @Override
                                                public void onNext(Boolean user) {
                                                    Log.i("task", "ok");
                                                }
                                            });
                                }
                            }
                        });
                    }else{
                        ViewGroup layout = (ViewGroup) refuse.getParent();
                        if (layout != null)
                            layout.removeView(refuse);
                    }
                } else {
                    /**
                     * si l'utilisateur est un client
                     */
                    ViewGroup layout2 = (ViewGroup) refuse.getParent();
                    if (layout2 != null)
                        layout2.removeView(refuse);
                    if (task.getState_mission() != 2 && task.getState_mission() != 3) {
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(context.getApplicationContext(), cancel.class);
                                intent.putExtra("task", task);
                                startActivity(intent);
                            }
                        });
                    } else {
                        ViewGroup layout = (ViewGroup) button.getParent();
                        if (layout != null)
                            layout.removeView(button);
                    }
                }

        }
        return view;
    }

}

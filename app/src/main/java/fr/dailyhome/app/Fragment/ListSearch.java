package fr.dailyhome.app.Fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import fr.dailyhome.app.Model.Search;
import fr.dailyhome.app.R;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListSearch extends Fragment {


    private View view;
    private Context context;
    private ListView listView;
    private EditText search;
    private Search searchValue;
    public ListSearch() {
        // Required empty public constructor
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.view = inflater.inflate(R.layout.fragment_list_search, container, false);
        this.context = view.getContext();
        this.listView = (ListView)view.findViewById(R.id.addressHistory);
        this.search = (EditText)view.findViewById(R.id.addressSearch);
        Bundle bundle = this.getArguments();
        if(searchValue==null)
            searchValue = bundle.getParcelable("search");
        search.setText(searchValue.getSearch());
        ArrayList<String> history = new ArrayList<>();
        history.add("Localisation");
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Search> query = realm.where(Search.class);
        RealmResults<Search> result1 = query.findAll();
        for (Search s: result1
             ) {
            Log.i("boucle", s.getSearch());
            history.add(s.getSearch());
        }
        realm.close();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, history);
        this.listView.setAdapter(adapter);
        /**
         * quand l'utilisateur click sur un élément de la list
         */
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Bundle bundle1 = new Bundle();
                Log.i("search", position+"");
                if(position==0){
                    Log.i("search", "ok");
                    bundle1.putBoolean("location", true);
                    searchValue.setSearch("GeoLocalisation");
                }else {

                    listView.getItemAtPosition(position);
                    String search = (String)listView.getItemAtPosition(position);
                    searchValue.setSearch(search);

                    bundle1.putBoolean("location", false);
                }
                bundle1.putParcelable("search", searchValue);
                SearchForm searchForm = new SearchForm();
                searchForm.setArguments(bundle1);
                fragmentTransaction.replace(R.id.createTaskLayout, searchForm);
                fragmentTransaction.commit();
            }
        });


        /**
         * quand le client sort du chant text
         */
        this.search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    searchValue.setSearch(search.getText().toString());
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    Search realmSearch = realm.copyToRealm(searchValue);
                    Log.i("boucle", realmSearch.getSearch());
                    realm.commitTransaction();
                    Bundle bundle1 = new Bundle();
                    bundle1.putParcelable("search", searchValue);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    SearchForm searchForm = new SearchForm();
                    searchForm.setArguments(bundle1);
                    fragmentTransaction.replace(R.id.createTaskLayout, searchForm);
                    fragmentTransaction.commit();
                }
            }
        });

        return this.view;
    }
}

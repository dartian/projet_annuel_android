package fr.dailyhome.app.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import fr.dailyhome.app.AccountUpdate;
import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountUser extends Fragment {


    private View view;
    private Context context;
    private TextView firstName;
    private TextView lastName;
    private TextView address;
    private TextView phoneNumber;
    private TextView eMail;
    private Button button;
    private User user;
    private Address addressUser;
    public AccountUser() {
        // Required empty public constructor
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_account_user, container, false);
        context = view.getContext();
        /**
         * initialisation des variables
         */
        firstName = (TextView)view.findViewById(R.id.firstName_acout);
        lastName = (TextView)view.findViewById(R.id.lastName_acount);
        address = (TextView)view.findViewById(R.id.addresse_acount);
        phoneNumber = (TextView)view.findViewById(R.id.phone_acount);
        eMail = (TextView)view.findViewById(R.id.eMail_acount);
        button = (Button)view.findViewById(R.id.acountModif);

        /**
         * initialisation realm
         */
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> result1 = query.findAll();
        user = result1.first();

        RealmQuery<Address> query1 = realm.where(Address.class);
        RealmResults<Address> results2 = query1.findAll();
        addressUser = results2.last();

        firstName.setText(user.getFirst_name());
        lastName.setText(user.getLast_name());
        phoneNumber.setText(user.getPhoneNumber());
        eMail.setText(user.geteMail());
        String displayAddre = addressUser.getAddress()+"\n"+addressUser.getZipCode()+" "+addressUser.getCity();
        Log.i("ACCOUNT", result1.size()+"");
        address.setText(displayAddre);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AccountUpdate.class));
            }
        });
        return view;
    }

}

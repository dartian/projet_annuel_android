package fr.dailyhome.app.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import fr.dailyhome.app.TaskActivity;
import fr.dailyhome.app.ViewHolder.TaskAdapter;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.RecyclerItemClickListener;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.TaskNetwork;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryMission extends Fragment {

    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private User user;
    private ArrayList<Task> taskList;
    public HistoryMission() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_history_mission, container, false);
        context = view.getContext();

        recyclerView = (RecyclerView) view.findViewById(R.id.listHistorique);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> result1 = query.findAll();
        user = result1.first();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener(){

                    @Override
                    public void onItemClick(View view, int position) {

                        /*Intent intent = new Intent(view.getContext(), TaskActivity.class);
                        intent.putExtra("FragmentTask", FragmentTask);
                        startActivity(intent);*/
                        Task task = taskList.get(position);
                        Intent intent = new Intent(view.getContext(), TaskActivity.class);
                        intent.putExtra("task", task);
                        startActivity(intent);
                    }
                })
        );
        getAppointment();
        return view;
    }

    /**
     * charge les missions et init la liste
     */
    public void getAppointment(){
        ConnectionUtility connectionUtility = new ConnectionUtility(context);
        if(connectionUtility.checkConnection()) {
            AppiConnexion appiConnexion = new AppiConnexion();
            TaskNetwork userService = appiConnexion.getRetrofit().create(TaskNetwork.class);
            Observable<List<Task>> user = userService.getHistorique(this.user.getId());

            user.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<List<Task>>() {
                        @Override
                        public void onCompleted() {


                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(context,
                                    R.string.error_message,
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }

                        @Override
                        public void onNext(List<Task> tasks) {
                            String[] type = context.getResources().getStringArray(R.array.taskType);
                            String[] state = context.getResources().getStringArray(R.array.state);
                            TaskAdapter taskAdapter = new TaskAdapter(tasks, type, state);
                            recyclerView.setAdapter(taskAdapter);
                            taskList = (ArrayList<Task>) tasks;
                        }

                    });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){

        }

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}

package fr.dailyhome.app.Fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import fr.dailyhome.app.Login;
import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.R;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.DatePickerFragment;
import fr.dailyhome.app.helper.Dates;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.UserService;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignFragment extends Fragment {
    private Calendar calendar = Calendar.getInstance();
    private String TAG = "SIGN_FRAGMENT";
    private Context context;
    private View view;
    private boolean insert = true;
    private EditText firstName;
    private EditText lastName;
    private EditText address;
    private EditText zipCode;
    private EditText eMail;
    private EditText password;
    private EditText password2;
    private EditText phoneNumber;
    private EditText city;


    private Button dateButton;
    private Button validButton;
    private Spinner civilitySpinner;
    private Spinner customerType;

    private Button valide;

    private Long time;

    public SignFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sign, container, false);
        context = view.getContext();
        civilitySpinner = (Spinner) view.findViewById(R.id.civility);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.civility, android.R.layout.simple_spinner_item);
        civilitySpinner.setAdapter(adapter);
        firstName = (EditText) this.view.findViewById(R.id.firstNameInput);
        lastName = (EditText) this.view.findViewById(R.id.lastNameInput);
        address = (EditText) this.view.findViewById(R.id.adressInput);
        zipCode = (EditText) this.view.findViewById(R.id.postalCodeInput);
        eMail = (EditText) this.view.findViewById(R.id.emailInput);
        password = (EditText) this.view.findViewById(R.id.paswordInput);
        password2 = (EditText) this.view.findViewById(R.id.passwordInput2);
        phoneNumber = (EditText) this.view.findViewById(R.id.phoneNumberInput);
        city = (EditText) this.view.findViewById(R.id.cityInput);

        dateButton = (Button) view.findViewById(R.id.birtDateButton);
        final Date minDate = new Date();
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment date = new DatePickerFragment(0, minDate.getTime()-1000) {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        calendar.set(y, m, d);
                        dateButton.setText(Dates.dateFormat.format(calendar.getTime()));
                        time = calendar.getTimeInMillis();
                    }
                };
                date.show(getFragmentManager().beginTransaction(), "datePicker");

            }
        });

        validButton = (Button) view.findViewById(R.id.signInButtonValid);
        validButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                /**
                 * Do the server communication for insert the new user + check if the data are correct
                 */

                String first_name = firstName.getText().toString();
                String last_name = lastName.getText().toString();
                String phone_number = phoneNumber.getText().toString();
                String e_Mail = eMail.getText().toString();
                String date = dateButton.getText().toString();
                String pass1 = password.getText().toString();
                String pass2 = password2.getText().toString();
                String add = address.getText().toString();
                String zip = zipCode.getText().toString();
                String ville = city.getText().toString();
                int civitlite = civilitySpinner.getSelectedItemPosition();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date dateUser = new Date(time);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    date =  simpleDateFormat.format(dateUser);

                if(!first_name.equals("") && !last_name.equals("") && !phone_number.equals("") && !e_Mail.equals("") && !date.equals("") && !pass1.equals("") && !pass2.equals("") && !add.equals("") && !zip.equals("") && !ville.equals("")) {
                    /**
                     * si toutes les valeurs sont renseigner
                     */
                    if (pass1.equals(pass2)) {
                        /**
                         * si les deux mots de passe sont identique
                         */
                        if (customerType.getSelectedItemPosition() == 1) {
                            /**
                             * si le client est un professionel on va à la deuxième étapes
                             */
                            Log.i("inscription", date);


                            User user = new User(-1, last_name, first_name, phone_number, e_Mail, new Address(-1, add, zip, ville, ""), 1, dateUser, "", pass1, civitlite);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("User", user);
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            SigninPro signinPro = new SigninPro();
                            signinPro.setArguments(bundle);
                            fragmentTransaction.replace(R.id.layoutSign, signinPro);
                            fragmentTransaction.commit();
                        } else {
                            /**
                             * si le client est un utilisateur normal on enregistre en base de donnée
                             */
                            ConnectionUtility connectionUtility = new ConnectionUtility(context);
                            if(connectionUtility.checkConnection()) {
                                AppiConnexion appiConnexion = new AppiConnexion();
                                UserService userService = appiConnexion.getRetrofit().create(UserService.class);
                                Log.i("add", firstName.getText().toString());

                                Observable<Boolean> check = userService.signIng(first_name, last_name, phone_number, e_Mail, date, pass1, add, zip, ville, 0, civitlite, "");
                                check.subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .timeout(5, TimeUnit.SECONDS)
                                        .unsubscribeOn(Schedulers.io())
                                        .subscribe(new Subscriber<Boolean>() {
                                            @Override
                                            public void onCompleted() {
                                                if (insert) {
                                                    Toast.makeText(context,
                                                            R.string.success_sign,
                                                            Toast.LENGTH_SHORT)
                                                            .show();
                                                    startActivity(new Intent(getActivity(), Login.class));
                                                    getActivity().finish();
                                                } else {
                                                    /**
                                                     * si l'e-mail est déjà utilisé
                                                     */
                                                    Toast.makeText(context,
                                                            "Cette e-mail est déjà utilisé",
                                                            Toast.LENGTH_SHORT)
                                                            .show();
                                                }
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                Toast.makeText(context,
                                                        R.string.error_message,
                                                        Toast.LENGTH_SHORT)
                                                        .show();
                                            }

                                            @Override
                                            public void onNext(Boolean user) {
                                                insert = user;
                                            }
                                        });

                            }
                        }
                    }
                }else{

                    Toast.makeText(context,
                            "Vous n'avez pas rentrer tout les champs",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });



        customerType = (Spinner) view.findViewById(R.id.typeUser);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(context, R.array.customerType, android.R.layout.simple_spinner_item);
        customerType.setAdapter(adapter1);





        return view;
    }

}

package fr.dailyhome.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.INetworkObservable;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.UserService;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AccountUpdate extends AppCompatActivity implements INetworkObservable {

    private EditText lastName;
    private EditText firstName;
    private EditText address;
    private EditText city;
    private EditText postalCode;
    private EditText phoneNumber;
    private EditText eMail;
    private Realm realm;
    private User user;
    private Address addressUser;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        lastName = (EditText)this.findViewById(R.id.updateLastName);
        firstName = (EditText)this.findViewById(R.id.updateFirstName);
        address = (EditText)this.findViewById(R.id.updateAddress);
        city = (EditText)this.findViewById(R.id.updateCity);
        postalCode = (EditText)this.findViewById(R.id.updatePostalCode);
        phoneNumber = (EditText)this.findViewById(R.id.updatePhone);
        eMail = (EditText)this.findViewById(R.id.updateEmail);


        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext()).name("user.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();

        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> result1 = query.findAll();
        user = result1.first();

        RealmQuery<Address> query1 = realm.where(Address.class);
        RealmResults<Address> results2 = query1.findAll();
        addressUser = results2.last();

        lastName.setText(user.getLast_name());
        firstName.setText(user.getFirst_name());
        address.setText(addressUser.getAddress());
        city.setText(addressUser.getCity());
        postalCode.setText(addressUser.getZipCode());
        phoneNumber.setText(user.getPhoneNumber());
        eMail.setText(user.geteMail());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_tache, menu);
        return true;
    }

    /**
     * envoie les moddification de comptes au server
     * @param view
     */
    public void updateAccount(View view) {
        //TODO implement the update of info information
        final Context context = this;
        AppiConnexion appiConnexion = new AppiConnexion();
        final UserService userService = appiConnexion.getRetrofit().create(UserService.class);
        final Observable<Boolean> userUpdate = userService.update(this.user.getId(), firstName.getText().toString(), lastName.getText().toString(), phoneNumber.getText().toString(), eMail.getText().toString(), address.getText().toString(), postalCode.getText().toString(), city.getText().toString());
        userUpdate.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Boolean>() {

                    @Override
                    public void onCompleted() {
                        startActivity(new Intent(context, ListTache.class));
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getApplicationContext(),
                                R.string.error_message,
                                Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if(aBoolean){
                            Log.i("UPDATE", "update");
                            realm.beginTransaction();
                            user.setFirst_name(firstName.getText().toString());
                            user.setLast_name(lastName.getText().toString());
                            user.setPhoneNumber(phoneNumber.getText().toString());
                            user.seteMail(eMail.getText().toString());
                            addressUser.setAddress(address.getText().toString());
                            addressUser.setCity(city.getText().toString());
                            addressUser.setZipCode(postalCode.getText().toString());
                            realm.commitTransaction();
                        }else{

                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_deco:
                // User chose the "Settings" item, show the app settings UI...
                final Context context = this;
                realm.executeTransaction(new Realm.Transaction(){

                    @Override
                    public void execute(Realm realm) {
                        RealmQuery<User> query = realm.where(User.class);
                        RealmResults<User> results = query.findAll();
                        RealmQuery<Address> query1 = realm.where(Address.class);
                        RealmResults<Address> results1 = query1.findAll();
                        results.deleteAllFromRealm();
                        results1.deleteAllFromRealm();
                        startActivity(new Intent(context, Login.class));
                        finish();
                    }
                });
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activityAccountUpdate);
        if(code == 0 && !change) {
            /**
             * affiche le message après avoir retrouvé une connection
             */
            Log.i("list", "connecter");
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Connection retrouvé", Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.GREEN);
            snackbar.show();
            change = true;

        }else if(code == 1){
            /**
             * affiche un message quand on pert la connection
             */
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            if(coordinatorLayout.getContext()!=null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Pas de connection", Snackbar.LENGTH_INDEFINITE);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.RED);
                snackbar.show();
                change = false;
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }
}

package fr.dailyhome.app;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;

import fr.dailyhome.app.Fragment.CreateTaskFirstStep;
import fr.dailyhome.app.Fragment.SearchForm;
import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.INetworkObservable;
import fr.dailyhome.app.helper.MigrationSearch;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class CreateTask extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, INetworkObservable {


    private GoogleApiClient googleApiClient;
    private Location location;
    private Realm realm;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        RealmConfiguration config = new RealmConfiguration.Builder(this).name("search.realm")
                .schemaVersion(1)
                .migration(new MigrationSearch())
                .build();
        Realm.setDefaultConfiguration(config);
        setContentView(R.layout.activity_create_task);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        HashMap<Integer, Fragment> fragmentHashMap = new HashMap<>();
        fragmentHashMap.put(0, new CreateTaskFirstStep());
        fragmentHashMap.put(1, new SearchForm());

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            //fragmentTransaction.add(R.id.createTaskLayout, createTaskFirstStep);
            int type = getIntent().getIntExtra("type", 0);
            fragmentTransaction.add(R.id.createTaskLayout, fragmentHashMap.get(type));
            fragmentTransaction.commit();
        }

        RealmConfiguration config2 = new RealmConfiguration.Builder(getApplicationContext()).name("user.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config2);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_tache, menu);
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("GPS", "Location services connected.");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            handleNewLocation(location);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    if(location!= null){
                        handleNewLocation(location);
                    }else{

                    }
                }
                break;
        }
    }

    private void handleNewLocation(Location location){
        Log.d("GPS", location.toString());
        
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount()>0){
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(getFragmentManager().getBackStackEntryCount()>0){
            getFragmentManager().popBackStack();
            return false;
        }else {
            getFragmentManager().popBackStack();
            return super.onSupportNavigateUp();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_deco:
                // User chose the "Settings" item, show the app settings UI...
                final Context context = this;
                realm.executeTransaction(new Realm.Transaction(){

                    @Override
                    public void execute(Realm realm) {
                        RealmQuery<User> query = realm.where(User.class);
                        RealmResults<User> results = query.findAll();
                        RealmQuery<Address> query1 = realm.where(Address.class);
                        RealmResults<Address> results1 = query1.findAll();
                        results.deleteAllFromRealm();
                        results1.deleteAllFromRealm();
                        startActivity(new Intent(context, Login.class));
                        finish();
                    }
                });

                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.createTask);
        if(code == 0 && !change) {
            Log.i("list", "connecter");
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Connection retrouvé", Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.GREEN);
            snackbar.show();
            change = true;

        }else if(code == 1){
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            if(coordinatorLayout.getContext()!=null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Pas de connection", Snackbar.LENGTH_INDEFINITE);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.RED);
                snackbar.show();
                change = false;
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }
}

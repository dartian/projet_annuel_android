package fr.dailyhome.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Task   implements Parcelable{

    private int id;
    private String name;
    private Date date_mission;
    private int id_user;
    private int id_pro;
    private int taskType;
    private String detail;
    private int paymentMethod;
    private int state;
    private int time_period;
    private int state_mission;
    private int repeat_type;
    private Address address;
    private int id_address;
    public Task(){
        name = "";
        date_mission = new Date();
        id_user = 0;
        taskType = 0;
        detail = "";
        paymentMethod = 0;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public Task(int id, String name, Date date, int id_user, int taskType, String detail, int state, int time_period, int state_mission, int repeat_type, int id_pro){
        this.id = id;
        this.name = name;
        this.date_mission = date;
        this.id_user = id_user;
        this.taskType = taskType;
        this.detail = detail;
        this.state = state;
        this.time_period = time_period;
        this.state_mission = state_mission;
        this.repeat_type = repeat_type;
        this.paymentMethod = 0;
        this.id_pro = id_pro;
    }

    protected Task(Parcel in) {
        this.setId(in.readInt());
        this.setName(in.readString());
        this.setTaskType(in.readInt());
        this.setDate(dateFromTimestamp(in.readLong()));
        this.setClient(in.readInt());
        this.setDetail(in.readString());
        this.setPaymentMethod(in.readInt());
        this.setState(in.readInt());
        this.setTime_period(in.readInt());
        this.setRepeat_type(in.readInt());
        this.setId_address(in.readInt());
        this.setState_mission(in.readInt());
        this.setId_pro(in.readInt());
        this.setId_user(in.readInt());

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.getId());
        dest.writeString(this.getName());
        dest.writeInt(this.getTaskType());
        dest.writeLong(this.getDate().getTime());
        dest.writeInt(this.getClient());
        dest.writeString(this.getDetail());
        dest.writeInt(this.getPaymentMethod());
        dest.writeInt(this.getState());
        dest.writeInt(this.getTime_period());
        dest.writeInt(this.getRepeat_type());
        dest.writeInt(this.getId_address());
        dest.writeInt(this.getState_mission());
        dest.writeInt(this.getId_pro());
        dest.writeInt(this.getId_user());
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Date dateFromTimestamp(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        return c.getTime();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date_mission;
    }

    public void setDate(Date date) {
        this.date_mission = date;
    }

    public int getClient() {
        return id_user;
    }

    public void setClient(int client) {
        this.id_user = client;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }


    public String getDateFormalise(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return simpleDateFormat.format(this.date_mission);
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getTime_period() {
        return time_period;
    }

    public void setTime_period(int time_period) {
        this.time_period = time_period;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRepeat_type() {
        return repeat_type;
    }

    public void setRepeat_type(int repeat_type) {
        this.repeat_type = repeat_type;
    }

    public int getId_address() {
        return id_address;
    }

    public void setId_address(int id_address) {
        this.id_address = id_address;
    }

    public int getState_mission() {
        return state_mission;
    }

    public void setState_mission(int state_mission) {
        this.state_mission = state_mission;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_pro() {
        return id_pro;
    }

    public void setId_pro(int id_pro) {
        this.id_pro = id_pro;
    }
}

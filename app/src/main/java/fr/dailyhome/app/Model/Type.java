package fr.dailyhome.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bidau on 28/02/2016.
 */
public class Type implements Parcelable {

    private String type;
    private boolean active;

    public Type(String type, boolean active) {
        this.type = type;
        this.active = active;
    }

    protected Type(Parcel in) {
        this.type = in.readString();
        this.active = in.readByte() != 0;
    }

    public static final Creator<Type> CREATOR = new Creator<Type>() {
        @Override
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        @Override
        public Type[] newArray(int size) {
            return new Type[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeByte((byte) (this.active ? 0 : 1));
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

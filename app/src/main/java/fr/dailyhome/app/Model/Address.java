package fr.dailyhome.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

public class Address extends RealmObject implements Parcelable {

    private int id;
    private String address;
    private String zipCode;
    private String city;
    private String country;


    public Address(){

    }

    public Address(int id, String address, String zipCode, String city, String country) {
        this.id = id;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }

    protected Address(Parcel in) {
        id = in.readInt();
        address = in.readString();
        zipCode = in.readString();
        city = in.readString();
        country = in.readString();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(address);
        dest.writeString(zipCode);
        dest.writeString(city);
        dest.writeString(country);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String toString(){
        return this.address+"\n"+this.zipCode +" "+this.city;
    }

    public boolean isEmpty(){
        return this.address==null && this.zipCode ==null && this.city == null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

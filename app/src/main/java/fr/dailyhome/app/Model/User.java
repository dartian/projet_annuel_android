package fr.dailyhome.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject implements Parcelable{

    @PrimaryKey
    private long user_id;
    private int id;
    private String last_name;
    private String first_name;
    private String phoneNumber;
    private String eMail;
    //private List<Task> taskList;
    private Address address;
    private int type;
    private Date birthDate;
    private String siret;
    private String password;
    private int civilite;

    public User(){
        //this.taskList = new ArrayList<>();
        this.birthDate = new Date();
        this.address = new Address();
    }

    public User(int id, String last_name, String first_name, String phoneNumber, String eMail, Address address, int type, Date birthDate, String siret, String password, int civilite) {

        this.id = id;
        this.last_name = last_name;
        this.first_name = first_name;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
        //this.taskList = taskList;
        this.address = address;
        this.type = type;
        this.birthDate = birthDate;
        this.siret = siret;
        this.password = password;

        this.civilite = civilite;
    }

    protected User(Parcel in) {
        id = in.readInt();
        last_name = in.readString();
        first_name = in.readString();
        phoneNumber = in.readString();
        eMail = in.readString();
        address = in.readParcelable(Address.class.getClassLoader());
        birthDate = new Date(in.readLong());
        siret = in.readString();
        password = in.readString();
        civilite = in.readInt();
    }


    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(user_id);
        dest.writeInt(id);
        dest.writeString(last_name);
        dest.writeString(first_name);
        dest.writeString(phoneNumber);
        dest.writeString(eMail);
        dest.writeParcelable(address, flags);
        dest.writeInt(type);
        dest.writeString(siret);
        dest.writeString(password);
        dest.writeInt(civilite);
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCivilite() {
        return civilite;
    }

    public void setCivilite(int civilite) {
        this.civilite = civilite;
    }
}

package fr.dailyhome.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

/**
 * Created by d.bidaud on 20/04/2016.
 */
public class Search extends RealmObject implements Parcelable  {

    private String search;
    private int type;

    public Search(){
        search = "";
        type = 0;
    }

    public Search(Parcel in) {
        search = in.readString();
        type = in.readInt();
    }

    public static final Creator<Search> CREATOR = new Creator<Search>() {
        @Override
        public Search createFromParcel(Parcel in) {
            return new Search(in);
        }

        @Override
        public Search[] newArray(int size) {
            return new Search[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(search);
        dest.writeInt(type);
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}

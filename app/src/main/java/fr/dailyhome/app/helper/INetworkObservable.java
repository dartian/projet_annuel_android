package fr.dailyhome.app.helper;

/**
 * Created by d.bidaud on 05/07/2016.
 */
public interface INetworkObservable {
    void updateStatus(int code);
}

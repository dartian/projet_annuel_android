package fr.dailyhome.app.helper;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by d.bidaud on 15/06/2016.
 */
public class UserMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if(oldVersion==0){
            schema.create("Address");
            oldVersion++;
        }
    }
}

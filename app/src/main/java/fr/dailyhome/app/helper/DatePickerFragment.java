package fr.dailyhome.app.helper;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;

import java.util.Calendar;

public abstract class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private long minDate;
    private long maxDate;


    public DatePickerFragment(){
        super();
    }

    public DatePickerFragment(long minDate, long maxDate){
        super();
        this.maxDate = maxDate;
        this.minDate = minDate;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        if(this.minDate>0)
            datePickerDialog.getDatePicker().setMinDate(this.minDate);
        else if(this.maxDate>0)
            datePickerDialog.getDatePicker().setMaxDate(this.maxDate);
        return datePickerDialog;
    }


    public long getMinDate() {
        return minDate;
    }

    public void setMinDate(long minDate) {
        this.minDate = minDate;
    }

    public long getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(long maxDate) {
        this.maxDate = maxDate;
    }
}
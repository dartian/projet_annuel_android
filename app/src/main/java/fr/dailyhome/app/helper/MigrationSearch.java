package fr.dailyhome.app.helper;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by d.bidaud on 21/04/2016.
 */
public class MigrationSearch implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        if (oldVersion == 1) {
            schema.create("Search")
                    .addField("search", String.class)
                    .addField("type", int.class);
            oldVersion++;
        }
        if (oldVersion == 0) {
            schema.create("Search")
                    .addField("search", String.class)
                    .addField("type", int.class);
            oldVersion++;
        }
    }
}

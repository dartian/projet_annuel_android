package fr.dailyhome.app.helper;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by d.bidaud on 05/07/2016.
 */
public class ConnectionUtility implements  Runnable{

    List<INetworkObservable> observerList;
    Context context;
    public ConnectionUtility(Context mContext) {
        this.context = mContext;
        observerList = new ArrayList<>();
    }

    /**
     * vérifie la connextion
     * @return
     */
    public boolean checkConnection(){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * vérifie la connextion
     * @param context
     * @return
     */
    public static boolean isConnectedToInternet(Context context) {

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * method utilisé dans le thread
     */
    @Override
    public void run() {
            if(isConnectedToInternet(context)){
                    notifyObservers(0);
            }
            else {
                notifyObservers(1);
            }
    }

    /**
     * ajoute un observer
     * @param o
     */
    public void addObserver(INetworkObservable o) {
        observerList.add(o);

    }


    /**
     * enleve un observer
     * @param o
     */
    public void removeObserver(INetworkObservable o) {
        observerList.remove(o);
    }

    /**
     * update tout les observer
     * @param code
     */
    public void notifyObservers(int code) {
        for(INetworkObservable networkObserver :  observerList) {
            networkObserver.updateStatus(code);
        }
    }
}

package fr.dailyhome.app.network;

import java.util.List;

import fr.dailyhome.app.Model.Task;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by d.bidaud on 14/06/2016.
 */
public interface TaskNetwork {

    @GET("appointment/allAppointment/{id}")
    Observable<List<Task>> getAppointement(@Path("id") int id);

    @GET("appointment/historique/{id}")
    Observable<List<Task>> getHistorique(@Path("id") int id);

    @GET("appointment/searchFromPosition/{latitude}/{longitude}/{type}")
    Observable<List<Task>> getSearchFromPosition(@Path("latitude") double latitude, @Path("longitude") double longitude, @Path("type") int type);

    @GET("appointment/searchFromAdd/{add}/{type}")
    Observable<List<Task>> getSearchFromAddress(@Path("add") String address, @Path("type") int type);



    @FormUrlEncoded
    @POST("appointment/create")
    Observable<Boolean> create(@Field("id_user") int id_user,@Field("id_address") int id_address, @Field("taskType") int taskType, @Field("detail") String detail, @Field("date") String date, @Field("paymentMethod") int paymentMethod, @Field("time_period") int time_period, @Field("repeat") int repeat);

    @FormUrlEncoded
    @POST("appointment/cancel")
    Observable<Boolean> cancel(@Field("id") int id, @Field("state") int state, @Field("date") String date, @Field("detail") String detail);


    @FormUrlEncoded
    @POST("appointment/accepted")
    Observable<Boolean> accepted(@Field("id") int id, @Field("id_task") int id_task);


    @FormUrlEncoded
    @POST("appointment/refuse")
    Observable<Boolean> refuse(@Field("id") int id);
}

package fr.dailyhome.app.network;


import fr.dailyhome.app.Model.Address;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by d.bidaud on 15/06/2016.
 */
public interface AddressService {

    @GET("address/getDefaultAddress/{id}")
    Observable<Address> getDefaultAddress(@Path("id") int id);

    @GET("address/getAddress/{id}")
    Observable<Address> getAddress(@Path("id") int id);

    @FormUrlEncoded
    @POST("address/create")
    Observable<Integer> createAddress(@Field("zipCode") String zipCode, @Field("city") String city, @Field("address") String address, @Field("id_user") int id);
}

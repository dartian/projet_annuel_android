package fr.dailyhome.app.network;

import fr.dailyhome.app.Model.User;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by bidau on 05/06/2016.
 */
public interface UserService {

    @GET("user/authenticate/{username}/{password}")
    Observable<User> authenticateUser(@Path("username") String username, @Path("password") String password);

    @GET("user/account/{username}")
    Observable<User> account(@Path("username") String username);

    @FormUrlEncoded
    @POST("user/signIn")
    Observable<Boolean> signIng(@Field("first_name") String first_name, @Field("last_name") String last_name, @Field("phoneNumber") String phoneNumber, @Field("eMail") String eMail, @Field("birthDate") String birthDate, @Field("password") String password, @Field("address")  String address, @Field("zipCode") String zipCode, @Field("city") String city, @Field("type") int type, @Field("civility") int civility, @Field("siret") String siret);

    @FormUrlEncoded
    @POST("user/update")
    Observable<Boolean> update(@Field("id") int id,@Field("first_name") String first_name, @Field("last_name") String last_name, @Field("phoneNumber")String phoneNumber, @Field("eMail") String eMail, @Field("address") String address, @Field("zipCode") String zipCode, @Field("city") String city);

    @FormUrlEncoded
    @POST("user/notifiedUpdate")
    Observable<Boolean> notified(@Field("id") int id, @Field("token") String token);
}

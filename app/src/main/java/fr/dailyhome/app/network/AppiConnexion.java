package fr.dailyhome.app.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bidau on 19/06/2016.
 */
public class AppiConnexion {

    private Retrofit retrofit;


    public AppiConnexion(){
        retrofit = new Retrofit.Builder()
                .baseUrl("http://damienbidaud.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public Retrofit getRetrofit(){
        return retrofit;
    }
}

package fr.dailyhome.app;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import fr.dailyhome.app.Fragment.FragmentTask;
import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.INetworkObservable;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class TaskActivity extends AppCompatActivity implements INetworkObservable {

    private Realm realm;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;

    private FragmentManager fragmentManager = null;
    private FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_task);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext()).name("user.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
        Intent intent = getIntent();
        Task task = intent.getParcelableExtra("task");
        Bundle bundle = new Bundle();
        bundle.putParcelable("task", task);
        if (savedInstanceState == null) {
            fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            FragmentTask fragmentTask = new FragmentTask();
            fragmentTask.setArguments(bundle);
            fragmentTransaction.add(R.id.layoutTask, fragmentTask, "ListFragment").commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_tache, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_deco:
                // User chose the "Settings" item, show the app settings UI...
                final Context context = this;
                realm.executeTransaction(new Realm.Transaction(){

                    @Override
                    public void execute(Realm realm) {
                        RealmQuery<User> query = realm.where(User.class);
                        RealmResults<User> results = query.findAll();
                        RealmQuery<Address> query1 = realm.where(Address.class);
                        RealmResults<Address> results1 = query1.findAll();
                        results.deleteAllFromRealm();
                        results1.deleteAllFromRealm();
                        startActivity(new Intent(context, Login.class));
                        finish();
                    }
                });

                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activityTask);
        if(code == 0 && !change) {
            Log.i("list", "connecter");
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Connection retrouvé", Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.GREEN);
            snackbar.show();
            change = true;

        }else if(code == 1){
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Pas de connection", Snackbar.LENGTH_INDEFINITE);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.RED);
            snackbar.show();
            change = false;
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }
}

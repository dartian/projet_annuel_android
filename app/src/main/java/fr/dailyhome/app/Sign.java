package fr.dailyhome.app;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import fr.dailyhome.app.Fragment.SignFragment;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.INetworkObservable;

public class Sign extends AppCompatActivity implements INetworkObservable {

    private FragmentManager fragmentManager = null;
    private FragmentTransaction fragmentTransaction;
    private SignFragment signPro;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            signPro = new SignFragment();
            fragmentTransaction.add(R.id.layoutSign, signPro, "ListFragment");
            fragmentTransaction.commit();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.signActivity);
        if(code == 0 && !change) {
            Log.i("list", "connecter");
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Connection retrouvé", Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.GREEN);
            snackbar.show();
            change = true;

        }else if(code == 1){
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            if(coordinatorLayout.getContext()!=null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Pas de connection", Snackbar.LENGTH_INDEFINITE);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.RED);
                snackbar.show();
                change = false;
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }
}

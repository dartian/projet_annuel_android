package fr.dailyhome.app;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.util.HashMap;
import java.util.Map;

import fr.dailyhome.app.Fragment.AccountUser;
import fr.dailyhome.app.Fragment.HistoryMission;
import fr.dailyhome.app.Fragment.ListTaskFragment;
import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.INetworkObservable;
import fr.dailyhome.app.network.AddressService;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.UserService;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ListTache extends AppCompatActivity implements INetworkObservable{

    private FragmentManager fragmentManager = null;
    private FragmentTransaction fragmentTransaction;
    private ListTaskFragment listTaskFragment;
    private BottomBar bottomBar;
    private Map<Integer, Fragment> listFragment;
    private static int MENU_POSITION = 1;
    private User user;
    private Realm realm;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext()).name("user.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        RealmResults<User> results = query.findAll();
        user =  results.get(0);
        RealmQuery<Address> query1 = realm.where(Address.class);
        //RealmResults<Address> results1 = query1.findAll();
        connectionUtility = new ConnectionUtility(this);
        if(query1.count()==1){
            if(connectionUtility.checkConnection()) {
                AppiConnexion appiConnexion = new AppiConnexion();
                AddressService addressService = appiConnexion.getRetrofit().create(AddressService.class);
                Observable<Address> address = addressService.getDefaultAddress(user.getId());

                address.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<Address>() {
                            @Override
                            public void onCompleted() {


                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(getApplicationContext(),
                                        R.string.error_message,
                                        Toast.LENGTH_SHORT)
                                        .show();
                                Log.i("error", e.getMessage());
                            }

                            @Override
                            public void onNext(Address address1) {
                                realm.beginTransaction();
                                final Address saveAddress = realm.copyToRealm(address1);
                                Log.i("add", saveAddress.toString());
                                realm.commitTransaction();
                            }
                        });
            }
        }

        sendToken();
        setContentView(R.layout.activity_list_tache);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listFragment = new HashMap<>();
        listFragment.put(R.id.accountMenu, new AccountUser());
        listFragment.put(R.id.homeMenu, new ListTaskFragment());
        listFragment.put(R.id.historyMenu, new HistoryMission());
        final Intent intent = getIntent();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(ListTache.this, CreateTask.class);
                if(user.getType()==1){
                    intent1.putExtra("type", 1);
                }else{
                    intent1.putExtra("type", 0);
                }
                startActivityForResult(intent1, 1);
            }

        });

        if (savedInstanceState == null) {
            fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            listTaskFragment = new ListTaskFragment();
            fragmentTransaction.add(R.id.layoutList, listTaskFragment, "ListFragment");
            fragmentTransaction.commit();
        }
        this.bottomBar = BottomBar.attach(this, savedInstanceState);
        this.bottomBar.setItemsFromMenu(R.menu.menu_bottom, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(int menuItemId) {
                fragmentManager = getFragmentManager();
                Fragment fragment = listFragment.get(menuItemId);
                if (fragment != null) {
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.layoutList, fragment);
                    fragmentTransaction.commit();
                }
            }

            @Override
            public void onMenuTabReSelected(int menuItemId) {

            }
        });
        if(savedInstanceState==null)
            this.bottomBar.setDefaultTabPosition(MENU_POSITION);
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_tache, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //thread.interrupt();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }

    @Override
    public void onSaveInstanceState(Bundle saveInstance){
        super.onSaveInstanceState(saveInstance);
        saveInstance.putInt("menuPosition", this.bottomBar.getCurrentTabPosition());
        this.bottomBar.onSaveInstanceState(saveInstance);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_deco:
                // User chose the "Settings" item, show the app settings UI...

                final Context context = this;
                realm.executeTransaction(new Realm.Transaction(){

                    @Override
                    public void execute(Realm realm) {
                        RealmQuery<User> query = realm.where(User.class);
                        RealmResults<User> results = query.findAll();
                        RealmQuery<Address> query1 = realm.where(Address.class);
                        RealmResults<Address> results1 = query1.findAll();
                        results.deleteAllFromRealm();
                        Log.i("DECO", results1.size()+"");
                        results1.deleteAllFromRealm();
                        Log.i("DECO", results1.size()+"");
                        startActivity(new Intent(context, Login.class));
                        finish();
                    }
                });
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * envoie le token de notification de l'appareille au server
     */
    public void sendToken(){
        String token = FirebaseInstanceId.getInstance().getToken();
        AppiConnexion appiConnexion = new AppiConnexion();
        UserService userService = appiConnexion.getRetrofit().create(UserService.class);
        Observable<Boolean> userNotified = userService.notified(user.getId(), token);
        Log.i("task", user.getId()+"");
        userNotified.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getApplicationContext(),
                                R.string.error_message,
                                Toast.LENGTH_SHORT)
                                .show();
                        Log.i("connexion error", e.getMessage());
                    }

                    @Override
                    public void onNext(Boolean bool) {

                    }
                });
    }

    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.listTask);
        if(code == 0 && !change) {
                Log.i("list", "connecter");
                assert coordinatorLayout != null;
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, R.string.connexionOk, Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.GREEN);
                snackbar.show();
                change = true;

        }else if(code == 1){
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            if(coordinatorLayout.getContext()!=null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, R.string.noConnexion, Snackbar.LENGTH_INDEFINITE);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.RED);
                snackbar.show();
                change = false;
            }
        }

    }
}

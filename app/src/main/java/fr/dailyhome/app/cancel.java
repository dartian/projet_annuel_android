package fr.dailyhome.app;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import fr.dailyhome.app.Model.Address;
import fr.dailyhome.app.Model.Task;
import fr.dailyhome.app.Model.User;
import fr.dailyhome.app.helper.ConnectionUtility;
import fr.dailyhome.app.helper.DatePickerFragment;
import fr.dailyhome.app.helper.Dates;
import fr.dailyhome.app.helper.INetworkObservable;
import fr.dailyhome.app.helper.TimePickerFragment;
import fr.dailyhome.app.network.AppiConnexion;
import fr.dailyhome.app.network.TaskNetwork;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class cancel extends AppCompatActivity implements INetworkObservable {

    private Calendar calendar = Calendar.getInstance();
    private Task task;
    private Realm realm;
    private Button dateSelect;
    private Button timeSelect;
    private EditText reason;
    private boolean change = true;
    private Thread thread;
    private ConnectionUtility connectionUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        task = intent.getParcelableExtra("task");
        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext()).name("user.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();

        dateSelect = (Button) this.findViewById(R.id.date_cancel);
        timeSelect = (Button) this.findViewById(R.id.time_cancel);
        reason = (EditText) this.findViewById(R.id.detailCancel);
        final Date minDate = new Date();
        dateSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment date = new DatePickerFragment(minDate.getTime()-1000, 0) {
                    @Override
                    public void onDateSet(DatePicker view, int y, int m, int d) {
                        calendar.set(y, m, d);
                        dateSelect.setText(Dates.dateFormat.format(calendar.getTime()));

                    }
                };

                date.show(getFragmentManager().beginTransaction(), "datePicker");
            }
        });

        timeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment time = new TimePickerFragment() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        timeSelect.setText(Dates.timeFormat.format(calendar.getTime()));
                    }
                };
                time.show(getFragmentManager(), "timePicker");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_tache, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.putExtra("FragmentTask", task);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's FragmentTask, so create a new FragmentTask
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                                    // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's FragmentTask, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                System.out.println("ok");
                return true;
            case R.id.action_deco:
                // User chose the "Settings" item, show the app settings UI...
                final Context context = this;
                realm.executeTransaction(new Realm.Transaction(){

                    @Override
                    public void execute(Realm realm) {
                        RealmQuery<User> query = realm.where(User.class);
                        RealmResults<User> results = query.findAll();
                        RealmQuery<Address> query1 = realm.where(Address.class);
                        RealmResults<Address> results1 = query1.findAll();
                        results.deleteAllFromRealm();
                        results1.deleteAllFromRealm();
                        startActivity(new Intent(context, Login.class));
                        finish();
                    }
                });
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public void cancelAction(View view){
        /**
         * send the cancellation to the server
         */
        String dateText = dateSelect.getText().toString();
        String TimeText = timeSelect.getText().toString();
        String dateLabel = this.getResources().getString(R.string.dateTaskTitle);
        String timeLabel = this.getResources().getString(R.string.newTaskTime);

        AppiConnexion appiConnexion = new AppiConnexion();
        TaskNetwork taskNetwork = appiConnexion.getRetrofit().create(TaskNetwork.class);
        Observable<Boolean> cancelAction;
        if(!dateText.equals(dateLabel) && !TimeText.equals(timeLabel)) {
            Log.i("cancel", "new date");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
            String dateInString = dateText + " " + TimeText + ":00";
            Date date = null;
            try {
                date = formatter.parse(dateInString);
                task.setDate(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.i("cancel", task.getId()+"");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString =  simpleDateFormat.format(date);
            cancelAction = taskNetwork.cancel(task.getId(), 0, dateString, reason.getText().toString());
        }else{

            cancelAction = taskNetwork.cancel(task.getId(), 2, "", reason.getText().toString());
        }
        cancelAction.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {
                        startActivity(new Intent(getApplicationContext(), ListTache.class));
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getApplicationContext(),
                                R.string.error_message,
                                Toast.LENGTH_SHORT)
                                .show();
                        Log.i("cancel", "error");
                    }

                    @Override
                    public void onNext(Boolean user) {

                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();


    }


    @Override
    public void updateStatus(int code) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activityCancel);
        if(code == 0 && !change) {
            Log.i("list", "connecter");
            assert coordinatorLayout != null;
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Connection retrouvé", Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.GREEN);
            snackbar.show();
            change = true;

        }else if(code == 1){
            Log.i("list", "pas connecter");
            Integer time = 10000;
            assert coordinatorLayout != null;
            if(coordinatorLayout.getContext()!=null) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Pas de connection", Snackbar.LENGTH_INDEFINITE);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(Color.RED);
                snackbar.show();
                change = false;
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        connectionUtility = new ConnectionUtility(getBaseContext());
        connectionUtility.addObserver(this);
        thread = new Thread(){
            @Override
            public void run() {
                while(!thread.isInterrupted()){
                    connectionUtility.run();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        connectionUtility.removeObserver(this);
    }
}
